# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging

# External imports
import numpy

logger = logging.getLogger()


class ExampleClient:
    """Example comms_client implementation

    During initialisation, it creates a correctly shaped zero filled value. on read that
    value is returned and on write its modified.
    """

    def start(self):
        return

    def __init__(self, fault_func, try_interval=2):
        """
        initialises the class and tries to connect to the client.
        """
        """ """
        self.fault_func = fault_func
        self.try_interval = try_interval
        self.stopping = False
        self.connected = False

        # super().__init__(daemon=True)

        # holder for the values of all attributes
        self.values = {}

        # Explicitly connect
        self.connect()

    def connect(self):
        """Connect to client"""
        self.connected = True  # set connected to true

    def disconnect(self):
        self.connected = (
            False  # always force a reconnect, regardless of a successful disconnect
        )
        logger.debug("disconnected from the 'client' ")

    def _setup_annotation(self, annotation):
        """Provide additional data to monitor/control point

        this function gives the client access to the comm client annotation data given
        to the attribute wrapper. The annotation data can be used to provide whatever
        extra data is necessary in order to find/access the monitor/control point.

        the annotation can be in whatever format may be required. it is up to the user
        to handle its content, example annotations may include:
        - a file path and file line/location
        - server address
        - IDs
        - data structures
        """

        # as this is an example, just print the annotation
        logger.debug("annotation: {}".format(annotation))

    def _setup_value_conversion(self, attribute):
        """
        gives the client access to the AttributeWrapper object in order to access all
        necessary data such as dimensionality and data type
        """

        if attribute.dim_y > 1:
            dims = (attribute.dim_y, attribute.dim_x)
        else:
            dims = (attribute.dim_x,)

        dtype = attribute.datatype

        return dims, dtype

    def _setup_mapping(self, annotation, dims, dtype):
        """Take gathered data to configure and return read and write functions"""

        # we emulate that values written to annotations ending in _RW show up in their
        # corresponding _R point as well
        if annotation.endswith("_RW"):
            annotation = annotation[:-1]

        if dtype == str and dims == (1,):
            self.values[annotation] = ""
        elif dims == (1,):
            self.values[annotation] = dtype(0)
        else:
            self.values[annotation] = numpy.zeros(dims, dtype)

        def read_function():
            logger.debug(
                "from read_function, reading {}: {} array of type {} == {}".format(
                    annotation, dims, dtype, self.values[annotation]
                )
            )
            return self.values[annotation]

        def write_function(write_value):
            logger.debug(
                "from write_function, writing {}: {} array of type {}".format(
                    annotation, dims, dtype
                )
            )

            self.values[annotation] = write_value

        logger.debug(
            "created and bound example_client read/write functions to AttributeWrapper "
            "object"
        )
        return read_function, write_function

    async def setup_attribute(self, annotation=None, attribute=None):
        """Return tuple with read and write functions

        MANDATORY function: is used by the attribute wrapper to get read/write
        functions. must return the read and write functions
        """

        # process the comms_annotation
        self._setup_annotation(annotation)

        # get all the necessary data to set up the read/write functions from the
        # AttributeWrapper
        dims, dtype = self._setup_value_conversion(attribute)

        # configure and return the read/write functions
        read_function, write_function = self._setup_mapping(annotation, dims, dtype)

        # return the read/write functions
        return read_function, write_function
