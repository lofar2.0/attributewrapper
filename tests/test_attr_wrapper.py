# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

""" test Device Server
"""

import asyncio
import unittest

import numpy
from unittest import TestCase
import testscenarios

# External imports
from tango import DevState, DevFailed, AttrWriteType
from tango.server import command, Device, DeviceMeta

# Test imports
from tango.test_context import DeviceTestContext

# Internal imports
from attribute_wrapper.attribute_wrapper import AttributeWrapper
from attribute_wrapper.states import INITIALISED_STATES
from example_client import ExampleClient

SCALAR_DIMS = (1,)
SPECTRUM_DIMS = (4,)
IMAGE_DIMS = (3, 2)

STR_SCALAR_VAL = "1"
STR_SPECTRUM_VAL = ["1", "1", "1", "1"]
STR_IMAGE_VAL = [["1", "1"], ["1", "1"], ["1", "1"]]


class DeviceWrapper(Device, metaclass=DeviceMeta):
    def __init__(self, cl, name):
        super().__init__(cl, name)

        self._attribute_wrapper_io = {}

    def is_attribute_access_allowed(self, req_type):
        """Returns whether an attribute wrapped by the AttributeWrapper be accessed."""

        return self.get_state() in INITIALISED_STATES

    @classmethod
    def attr_list(cls):
        """Return a list of all the AttributeWrapper members of this class."""
        return [v for k, v in cls.__dict__.items() if isinstance(v, AttributeWrapper)]

    @command()
    def off(self):
        self.set_state(DevState.OFF)

    @command()
    def on(self):
        self.set_state(DevState.ON)

    @command()
    def Fault(self):
        self.set_state(DevState.FAULT)

    def configure_for_initialise(self):
        self.set_state(DevState.INIT)
        self.example_client = ExampleClient(self.Fault)

        for i in self.attr_list():
            asyncio.run(i.async_set_comm_client(self, self.example_client))
            self.example_client.start()

    @command()
    def initialise(self):
        self.configure_for_initialise()


class TestAttributeTypes(testscenarios.WithScenarios, unittest.TestCase):
    class StrScalarDevice(DeviceWrapper):
        scalar_R = AttributeWrapper(comms_annotation="str_scalar_R", datatype=str)
        scalar_RW = AttributeWrapper(
            comms_annotation="str_scalar_RW",
            datatype=str,
            access=AttrWriteType.READ_WRITE,
        )

    class BoolScalarDevice(DeviceWrapper):
        scalar_R = AttributeWrapper(comms_annotation="bool_scalar_R", datatype=bool)
        scalar_RW = AttributeWrapper(
            comms_annotation="bool_scalar_RW",
            datatype=bool,
            access=AttrWriteType.READ_WRITE,
        )

    class Float32ScalarDevice(DeviceWrapper):
        scalar_R = AttributeWrapper(
            comms_annotation="float32_scalar_R", datatype=numpy.float32
        )
        scalar_RW = AttributeWrapper(
            comms_annotation="float32_scalar_RW",
            datatype=numpy.float32,
            access=AttrWriteType.READ_WRITE,
        )

    class Float64ScalarDevice(DeviceWrapper):
        scalar_R = AttributeWrapper(
            comms_annotation="float64_scalar_R", datatype=numpy.float64
        )
        scalar_RW = AttributeWrapper(
            comms_annotation="float64_scalar_RW",
            datatype=numpy.float64,
            access=AttrWriteType.READ_WRITE,
        )

    class DoubleScalarDevice(DeviceWrapper):
        scalar_R = AttributeWrapper(
            comms_annotation="double_scalar_R", datatype=numpy.double
        )
        scalar_RW = AttributeWrapper(
            comms_annotation="double_scalar_RW",
            datatype=numpy.double,
            access=AttrWriteType.READ_WRITE,
        )

    class Uint8ScalarDevice(DeviceWrapper):
        scalar_R = AttributeWrapper(
            comms_annotation="uint8_scalar_R", datatype=numpy.uint8
        )
        scalar_RW = AttributeWrapper(
            comms_annotation="uint8_scalar_RW",
            datatype=numpy.uint8,
            access=AttrWriteType.READ_WRITE,
        )

    class Uint16ScalarDevice(DeviceWrapper):
        scalar_R = AttributeWrapper(
            comms_annotation="uint16_scalar_R", datatype=numpy.uint16
        )
        scalar_RW = AttributeWrapper(
            comms_annotation="uint16_scalar_RW",
            datatype=numpy.uint16,
            access=AttrWriteType.READ_WRITE,
        )

    class Uint32ScalarDevice(DeviceWrapper):
        scalar_R = AttributeWrapper(
            comms_annotation="uint32_scalar_R", datatype=numpy.uint32
        )
        scalar_RW = AttributeWrapper(
            comms_annotation="uint32_scalar_RW",
            datatype=numpy.uint32,
            access=AttrWriteType.READ_WRITE,
        )

    class Uint64ScalarDevice(DeviceWrapper):
        scalar_R = AttributeWrapper(
            comms_annotation="uint64_scalar_R", datatype=numpy.uint64
        )
        scalar_RW = AttributeWrapper(
            comms_annotation="uint64_scalar_RW",
            datatype=numpy.uint64,
            access=AttrWriteType.READ_WRITE,
        )

    class Int16ScalarDevice(DeviceWrapper):
        scalar_R = AttributeWrapper(
            comms_annotation="int16_scalar_R", datatype=numpy.int16
        )
        scalar_RW = AttributeWrapper(
            comms_annotation="int16_scalar_RW",
            datatype=numpy.int16,
            access=AttrWriteType.READ_WRITE,
        )

    class Int32ScalarDevice(DeviceWrapper):
        scalar_R = AttributeWrapper(
            comms_annotation="int32_scalar_R", datatype=numpy.int32
        )
        scalar_RW = AttributeWrapper(
            comms_annotation="int32_scalar_RW",
            datatype=numpy.int32,
            access=AttrWriteType.READ_WRITE,
        )

    class Int64ScalarDevice(DeviceWrapper):
        scalar_R = AttributeWrapper(
            comms_annotation="int64_scalar_R", datatype=numpy.int64
        )
        scalar_RW = AttributeWrapper(
            comms_annotation="int64_scalar_RW",
            datatype=numpy.int64,
            access=AttrWriteType.READ_WRITE,
        )

    class StrSpectrumDevice(DeviceWrapper):
        spectrum_R = AttributeWrapper(
            comms_annotation="str_spectrum_R", datatype=str, dims=SPECTRUM_DIMS
        )
        spectrum_RW = AttributeWrapper(
            comms_annotation="str_spectrum_RW",
            datatype=str,
            access=AttrWriteType.READ_WRITE,
            dims=SPECTRUM_DIMS,
        )

    class BoolSpectrumDevice(DeviceWrapper):
        spectrum_R = AttributeWrapper(
            comms_annotation="bool_spectrum_R", datatype=bool, dims=SPECTRUM_DIMS
        )
        spectrum_RW = AttributeWrapper(
            comms_annotation="bool_spectrum_RW",
            datatype=bool,
            access=AttrWriteType.READ_WRITE,
            dims=SPECTRUM_DIMS,
        )

    class Float32SpectrumDevice(DeviceWrapper):
        spectrum_R = AttributeWrapper(
            comms_annotation="float32_spectrum_R",
            datatype=numpy.float32,
            dims=SPECTRUM_DIMS,
        )
        spectrum_RW = AttributeWrapper(
            comms_annotation="float32_spectrum_RW",
            datatype=numpy.float32,
            access=AttrWriteType.READ_WRITE,
            dims=SPECTRUM_DIMS,
        )

    class Float64SpectrumDevice(DeviceWrapper):
        spectrum_R = AttributeWrapper(
            comms_annotation="float64_spectrum_R",
            datatype=numpy.float64,
            dims=SPECTRUM_DIMS,
        )
        spectrum_RW = AttributeWrapper(
            comms_annotation="float64_spectrum_RW",
            datatype=numpy.float64,
            access=AttrWriteType.READ_WRITE,
            dims=SPECTRUM_DIMS,
        )

    class DoubleSpectrumDevice(DeviceWrapper):
        spectrum_R = AttributeWrapper(
            comms_annotation="double_spectrum_R",
            datatype=numpy.double,
            dims=SPECTRUM_DIMS,
        )
        spectrum_RW = AttributeWrapper(
            comms_annotation="double_spectrum_RW",
            datatype=numpy.double,
            access=AttrWriteType.READ_WRITE,
            dims=SPECTRUM_DIMS,
        )

    class Uint8SpectrumDevice(DeviceWrapper):
        spectrum_R = AttributeWrapper(
            comms_annotation="uint8_spectrum_R",
            datatype=numpy.uint8,
            dims=SPECTRUM_DIMS,
        )
        spectrum_RW = AttributeWrapper(
            comms_annotation="uint8_spectrum_RW",
            datatype=numpy.uint8,
            access=AttrWriteType.READ_WRITE,
            dims=SPECTRUM_DIMS,
        )

    class Uint16SpectrumDevice(DeviceWrapper):
        spectrum_R = AttributeWrapper(
            comms_annotation="uint16_spectrum_R",
            datatype=numpy.uint16,
            dims=SPECTRUM_DIMS,
        )
        spectrum_RW = AttributeWrapper(
            comms_annotation="uint16_spectrum_RW",
            datatype=numpy.uint16,
            access=AttrWriteType.READ_WRITE,
            dims=SPECTRUM_DIMS,
        )

    class Uint32SpectrumDevice(DeviceWrapper):
        spectrum_R = AttributeWrapper(
            comms_annotation="uint32_spectrum_R",
            datatype=numpy.uint32,
            dims=SPECTRUM_DIMS,
        )
        spectrum_RW = AttributeWrapper(
            comms_annotation="uint32_spectrum_RW",
            datatype=numpy.uint32,
            access=AttrWriteType.READ_WRITE,
            dims=SPECTRUM_DIMS,
        )

    class Uint64SpectrumDevice(DeviceWrapper):
        spectrum_R = AttributeWrapper(
            comms_annotation="uint64_spectrum_R",
            datatype=numpy.uint64,
            dims=SPECTRUM_DIMS,
        )
        spectrum_RW = AttributeWrapper(
            comms_annotation="uint64_spectrum_RW",
            datatype=numpy.uint64,
            access=AttrWriteType.READ_WRITE,
            dims=SPECTRUM_DIMS,
        )

    class Int16SpectrumDevice(DeviceWrapper):
        spectrum_R = AttributeWrapper(
            comms_annotation="int16_spectrum_R",
            datatype=numpy.int16,
            dims=SPECTRUM_DIMS,
        )
        spectrum_RW = AttributeWrapper(
            comms_annotation="int16_spectrum_RW",
            datatype=numpy.int16,
            access=AttrWriteType.READ_WRITE,
            dims=SPECTRUM_DIMS,
        )

    class Int32SpectrumDevice(DeviceWrapper):
        spectrum_R = AttributeWrapper(
            comms_annotation="int32_spectrum_R",
            datatype=numpy.int32,
            dims=SPECTRUM_DIMS,
        )
        spectrum_RW = AttributeWrapper(
            comms_annotation="int32_spectrum_RW",
            datatype=numpy.int32,
            access=AttrWriteType.READ_WRITE,
            dims=SPECTRUM_DIMS,
        )

    class Int64SpectrumDevice(DeviceWrapper):
        spectrum_R = AttributeWrapper(
            comms_annotation="int64_spectrum_R",
            datatype=numpy.int64,
            dims=SPECTRUM_DIMS,
        )
        spectrum_RW = AttributeWrapper(
            comms_annotation="int64_spectrum_RW",
            datatype=numpy.int64,
            access=AttrWriteType.READ_WRITE,
            dims=SPECTRUM_DIMS,
        )

    class StrImageDevice(DeviceWrapper):
        image_R = AttributeWrapper(
            comms_annotation="str_image_R", datatype=str, dims=(3, 2)
        )
        image_RW = AttributeWrapper(
            comms_annotation="str_image_RW",
            datatype=str,
            access=AttrWriteType.READ_WRITE,
            dims=(3, 2),
        )

    class BoolImageDevice(DeviceWrapper):
        image_R = AttributeWrapper(
            comms_annotation="bool_image_R", datatype=bool, dims=(3, 2)
        )
        image_RW = AttributeWrapper(
            comms_annotation="bool_image_RW",
            datatype=bool,
            access=AttrWriteType.READ_WRITE,
            dims=(3, 2),
        )

    class Float32ImageDevice(DeviceWrapper):
        image_R = AttributeWrapper(
            comms_annotation="float32_image_R", datatype=numpy.float32, dims=(3, 2)
        )
        image_RW = AttributeWrapper(
            comms_annotation="float32_image_RW",
            datatype=numpy.float32,
            access=AttrWriteType.READ_WRITE,
            dims=(3, 2),
        )

    class Float64ImageDevice(DeviceWrapper):
        image_R = AttributeWrapper(
            comms_annotation="float64_image_R", datatype=numpy.float64, dims=(3, 2)
        )
        image_RW = AttributeWrapper(
            comms_annotation="float64_image_RW",
            datatype=numpy.float64,
            access=AttrWriteType.READ_WRITE,
            dims=(3, 2),
        )

    class DoubleImageDevice(DeviceWrapper):
        image_R = AttributeWrapper(
            comms_annotation="double_image_R", datatype=numpy.double, dims=(3, 2)
        )
        image_RW = AttributeWrapper(
            comms_annotation="double_image_RW",
            datatype=numpy.double,
            access=AttrWriteType.READ_WRITE,
            dims=(3, 2),
        )

    class Uint8ImageDevice(DeviceWrapper):
        image_R = AttributeWrapper(
            comms_annotation="uint8_image_R", datatype=numpy.uint8, dims=(3, 2)
        )
        image_RW = AttributeWrapper(
            comms_annotation="uint8_image_RW",
            datatype=numpy.uint8,
            access=AttrWriteType.READ_WRITE,
            dims=(3, 2),
        )

    class Uint16ImageDevice(DeviceWrapper):
        image_R = AttributeWrapper(
            comms_annotation="uint16_image_R", datatype=numpy.uint16, dims=(3, 2)
        )
        image_RW = AttributeWrapper(
            comms_annotation="uint16_image_RW",
            datatype=numpy.uint16,
            access=AttrWriteType.READ_WRITE,
            dims=(3, 2),
        )

    class Uint32ImageDevice(DeviceWrapper):
        image_R = AttributeWrapper(
            comms_annotation="uint32_image_R", datatype=numpy.uint32, dims=(3, 2)
        )
        image_RW = AttributeWrapper(
            comms_annotation="uint32_image_RW",
            datatype=numpy.uint32,
            access=AttrWriteType.READ_WRITE,
            dims=(3, 2),
        )

    class Uint64ImageDevice(DeviceWrapper):
        image_R = AttributeWrapper(
            comms_annotation="uint64_image_R", datatype=numpy.uint64, dims=(3, 2)
        )
        image_RW = AttributeWrapper(
            comms_annotation="uint64_image_RW",
            datatype=numpy.uint64,
            access=AttrWriteType.READ_WRITE,
            dims=(3, 2),
        )

    class Int16ImageDevice(DeviceWrapper):
        image_R = AttributeWrapper(
            comms_annotation="int16_image_R", datatype=numpy.int16, dims=(3, 2)
        )
        image_RW = AttributeWrapper(
            comms_annotation="int16_image_RW",
            datatype=numpy.int16,
            access=AttrWriteType.READ_WRITE,
            dims=(3, 2),
        )

    class Int32ImageDevice(DeviceWrapper):
        image_R = AttributeWrapper(
            comms_annotation="int32_image_R", datatype=numpy.int32, dims=(3, 2)
        )
        image_RW = AttributeWrapper(
            comms_annotation="int32_image_RW",
            datatype=numpy.int32,
            access=AttrWriteType.READ_WRITE,
            dims=(3, 2),
        )

    class Int64ImageDevice(DeviceWrapper):
        image_R = AttributeWrapper(
            comms_annotation="int64_image_R", datatype=numpy.int64, dims=(3, 2)
        )
        image_RW = AttributeWrapper(
            comms_annotation="int64_image_RW",
            datatype=numpy.int64,
            access=AttrWriteType.READ_WRITE,
            dims=(3, 2),
        )

    def read_R_test(self, dev, dtype, test_type):
        """Test device"""
        with DeviceTestContext(dev, process=True) as proxy:
            # initialise
            proxy.initialise()
            proxy.on()

            if test_type == "scalar":
                expected = numpy.zeros((1,), dtype=dtype)
                val = proxy.scalar_R
            elif test_type == "spectrum":
                expected = numpy.zeros(SPECTRUM_DIMS, dtype=dtype)
                val = proxy.spectrum_R
            elif test_type == "image":
                expected = numpy.zeros(IMAGE_DIMS, dtype=dtype)
                val = numpy.array(
                    proxy.image_R
                )  # is needed for STR since they act differently

                # cant use all() for 2d arrays so instead compare the dimensions and
                # then flatten to 2d
                self.assertEqual(
                    val.shape,
                    expected.shape,
                    "image R array dimensions got mangled. Expected {}, got {}".format(
                        expected.shape, val.shape
                    ),
                )
                val.reshape(-1)
            else:
                self.assertEqual(
                    1,
                    2,
                    "{} is not a valid test_type. please use either scalar,"
                    "spectrum or image".format(test_type),
                )

            if test_type == "scalar":
                comparison = expected == val
                self.assertTrue(
                    comparison,
                    "Value could not be read or was not what was expected. Expected:"
                    "{}, got {}".format(expected, val),
                )
            else:
                comparison = expected == val
                equal_arrays = comparison.all()
                self.assertTrue(
                    equal_arrays,
                    " Value could not be read or was not what was expected. Expected:"
                    "{}, got {}".format(expected, val),
                )

            print(
                " Test passed! Managed to read R attribute value. got: {}".format(val)
            )

    def write_RW_test(self, dev, dtype, test_type):
        """Test device"""

        with DeviceTestContext(dev, process=True) as proxy:
            # initialise
            proxy.initialise()
            proxy.on()

            if test_type == "scalar":
                if dtype is str:
                    val = STR_SCALAR_VAL
                else:
                    val = dtype(1)
                proxy.scalar_RW = val
            elif test_type == "spectrum":
                if dtype is str:
                    val = STR_SPECTRUM_VAL
                else:
                    val = numpy.full(SPECTRUM_DIMS, dtype=dtype, fill_value=1)
                print(val)
                proxy.spectrum_RW = val
            elif test_type == "image":
                if dtype is str:
                    val = STR_IMAGE_VAL
                else:
                    val = numpy.full(IMAGE_DIMS, dtype=dtype, fill_value=1)
                proxy.image_RW = val
            else:
                self.assertEqual(
                    1,
                    2,
                    " {} is not a valid test_type. please use either scalar,"
                    "spectrum or image".format(test_type),
                )

    def read_RW_test(self, dev, dtype, test_type):
        """Test device"""
        expected = None
        val = None

        try:
            with DeviceTestContext(dev, process=True) as proxy:
                # initialise
                proxy.initialise()
                proxy.on()

                if test_type == "scalar":
                    expected = numpy.zeros((1,), dtype=dtype)
                    val = proxy.scalar_RW
                elif test_type == "spectrum":
                    expected = numpy.zeros(SPECTRUM_DIMS, dtype=dtype)
                    val = proxy.spectrum_RW
                elif test_type == "image":
                    expected = numpy.zeros(IMAGE_DIMS, dtype=dtype)
                    val = numpy.array(
                        proxy.image_RW
                    )  # is needed for STR since they act differently

                    # cant use all() for 2d arrays so instead compare the dimensions and
                    # then flatten to 2d
                    self.assertEqual(
                        val.shape,
                        expected.shape,
                        "image R array dimensions got mangled. Expected {},"
                        "got {}".format(expected.shape, val.shape),
                    )
                    val.reshape(-1)
                else:
                    self.assertEqual(
                        1,
                        2,
                        " {} is not a valid test_type. please use either scalar,"
                        "spectrum or image".format(test_type),
                    )

                if test_type != "scalar":
                    # spectrums and the now flattened images can be compared with .all()
                    comparison = expected == val
                    equal_arrays = comparison.all()
                    self.assertTrue(
                        equal_arrays,
                        "Value could not be handled by the atrribute_wrappers internal"
                        "RW storer",
                    )
                else:
                    comparison = expected == val
                    self.assertTrue(
                        comparison,
                        "Value could not be handled by the atrribute_wrappers internal"
                        "RW storer",
                    )

                print(
                    " Test passed! Managed to read internal RW value. got: {}".format(
                        val
                    )
                )
        except Exception as e:
            info = "Test failure in {} {} read RW test. Expected: {}, got {}".format(
                test_type, dtype, expected, val
            )
            raise Exception(info) from e

    def _get_result_type(self, dtype, test_type, proxy):
        if test_type == "scalar":
            if dtype is str:
                val = STR_SCALAR_VAL
            else:
                val = dtype(1)
            proxy.scalar_RW = val
            result_R = proxy.scalar_R
            result_RW = proxy.scalar_RW
        elif test_type == "spectrum":
            if dtype is str:
                val = STR_SPECTRUM_VAL
            else:
                val = numpy.full(SPECTRUM_DIMS, dtype=dtype, fill_value=1)
            proxy.spectrum_RW = val
            result_R = proxy.spectrum_R
            result_RW = proxy.spectrum_RW
        elif test_type == "image":
            if dtype is str:
                val = STR_IMAGE_VAL
            else:
                val = numpy.full(IMAGE_DIMS, dtype=dtype, fill_value=1)

            # info += " write value: {}".format(val)
            proxy.image_RW = val
            result_R = proxy.image_R
            result_RW = proxy.image_RW

            if dtype != str:
                self.assertEqual(
                    result_R.shape, IMAGE_DIMS, "not the correct dimensions"
                )

                result_R = result_R.reshape(-1)
                result_RW = result_RW.reshape(-1)
                val = val.reshape(-1)

        else:
            # if the test isn't scalar/spectrum or image its wrong
            self.assertEqual(
                1,
                2,
                "{} is not a valid test_type. please use either scalar,"
                "spectrum or image".format(test_type),
            )

        return result_R, result_RW, val

    def readback_test(self, dev, dtype, test_type):
        """Test device"""
        try:
            with DeviceTestContext(dev, process=True) as proxy:
                # initialise
                proxy.initialise()
                proxy.on()

                # get result and val
                result_R, result_RW, val = self._get_result_type(
                    dtype, test_type, proxy
                )

                if test_type == "scalar":
                    comparison = result_RW == val
                    self.assertTrue(
                        comparison,
                        "Value could not be handled by the atrribute_wrappers internal "
                        "RW storer. attempted to write: {}".format(val),
                    )
                    comparison = result_R == val
                    self.assertTrue(
                        comparison,
                        "value in the clients R attribute not equal to what was"
                        "written. read: {}, wrote {}".format(result_R, val),
                    )
                elif dtype != str:
                    comparison = result_RW == val
                    equal_arrays = comparison.all()
                    self.assertTrue(
                        equal_arrays,
                        "Value could not be handled by the atrribute_wrappers internal "
                        "RW storer. attempted to write: {}".format(val),
                    )
                    comparison = result_R == val
                    equal_arrays = comparison.all()
                    self.assertTrue(
                        equal_arrays,
                        "value in the clients R attribute not equal to what was "
                        "written. read: {}, wrote {}".format(result_R, val),
                    )
                else:
                    if test_type == "image":
                        self.assertEqual(
                            len(result_RW) * len(result_RW[0]),
                            6,
                            "array dimensions do not match the expected dimensions."
                            "expected {}, got: {}".format(
                                val, len(result_RW) * len(result_RW[0])
                            ),
                        )
                        self.assertEqual(
                            len(result_RW) * len(result_RW[0]),
                            6,
                            "array dimensions do not match the expected dimensions."
                            "expected {}, got: {}".format(
                                val, len(result_R) * len([0])
                            ),
                        )
                    else:
                        self.assertEqual(
                            len(result_RW),
                            4,
                            "array dimensions do not match the expected dimensions."
                            "expected {}, got: {}".format(4, len(result_RW)),
                        )
                        self.assertEqual(
                            len(result_R),
                            4,
                            "array dimensions do not match the expected dimensions."
                            "expected {}, got: {}".format(4, len(result_R)),
                        )

                print(
                    " Test passed! Managed write and read back a value: {}".format(val)
                )

        except Exception as e:
            info = {
                f"Test failure in {test_type} {dtype} readback test "
                f"\n\tW: {val} \n\tRW: {result_RW} \n\tR: {result_R}"
            }
            raise Exception(info) from e

    """
    List of different types to be used with attributes testing, using any other
    might have unexpected results. Each type is bound to a device scalar,
    spectrum and image class
    """
    ATTRIBUTE_TYPE_TESTS = [
        {
            "type": str,
            "scalar": StrScalarDevice,
            "spectrum": StrSpectrumDevice,
            "image": StrImageDevice,
        },
        {
            "type": bool,
            "scalar": BoolScalarDevice,
            "spectrum": BoolSpectrumDevice,
            "image": BoolImageDevice,
        },
        {
            "type": numpy.float32,
            "scalar": Float32ScalarDevice,
            "spectrum": Float32SpectrumDevice,
            "image": Float32ImageDevice,
        },
        {
            "type": numpy.float64,
            "scalar": Float64ScalarDevice,
            "spectrum": Float64SpectrumDevice,
            "image": Float64ImageDevice,
        },
        {
            "type": numpy.double,
            "scalar": DoubleScalarDevice,
            "spectrum": DoubleSpectrumDevice,
            "image": DoubleImageDevice,
        },
        {
            "type": numpy.uint8,
            "scalar": Uint8ScalarDevice,
            "spectrum": Uint8SpectrumDevice,
            "image": Uint8ImageDevice,
        },
        {
            "type": numpy.uint16,
            "scalar": Uint16ScalarDevice,
            "spectrum": Uint16SpectrumDevice,
            "image": Uint16ImageDevice,
        },
        {
            "type": numpy.uint32,
            "scalar": Uint32ScalarDevice,
            "spectrum": Uint32SpectrumDevice,
            "image": Uint32ImageDevice,
        },
        {
            "type": numpy.uint64,
            "scalar": Uint64ScalarDevice,
            "spectrum": Uint64SpectrumDevice,
            "image": Uint64ImageDevice,
        },
        {
            "type": numpy.int16,
            "scalar": Int16ScalarDevice,
            "spectrum": Int16SpectrumDevice,
            "image": Int16ImageDevice,
        },
        {
            "type": numpy.int32,
            "scalar": Int32ScalarDevice,
            "spectrum": Int32SpectrumDevice,
            "image": Int32ImageDevice,
        },
        {
            "type": numpy.int64,
            "scalar": Int64ScalarDevice,
            "spectrum": Int64SpectrumDevice,
            "image": Int64ImageDevice,
        },
    ]

    def test_scalar_R(self):
        for attribute_type_test in self.ATTRIBUTE_TYPE_TESTS:
            self.read_R_test(
                attribute_type_test["scalar"], attribute_type_test["type"], "scalar"
            )

    def test_scalar_RW(self):
        for attribute_type_test in self.ATTRIBUTE_TYPE_TESTS:
            self.read_RW_test(
                attribute_type_test["scalar"], attribute_type_test["type"], "scalar"
            )

    def test_scalar_W(self):
        for attribute_type_test in self.ATTRIBUTE_TYPE_TESTS:
            self.write_RW_test(
                attribute_type_test["scalar"], attribute_type_test["type"], "scalar"
            )

    def test_scalar_readback(self):
        for attribute_type_test in self.ATTRIBUTE_TYPE_TESTS:
            self.readback_test(
                attribute_type_test["scalar"], attribute_type_test["type"], "scalar"
            )

    def test_spectrum_R(self):
        for attribute_type_test in self.ATTRIBUTE_TYPE_TESTS:
            self.read_R_test(
                attribute_type_test["spectrum"], attribute_type_test["type"], "spectrum"
            )

    def test_spectrum_RW(self):
        for attribute_type_test in self.ATTRIBUTE_TYPE_TESTS:
            self.read_RW_test(
                attribute_type_test["spectrum"], attribute_type_test["type"], "spectrum"
            )

    def test_spectrum_W(self):
        for attribute_type_test in self.ATTRIBUTE_TYPE_TESTS:
            self.write_RW_test(
                attribute_type_test["spectrum"], attribute_type_test["type"], "spectrum"
            )

    def test_spectrum_readback(self):
        for attribute_type_test in self.ATTRIBUTE_TYPE_TESTS:
            self.readback_test(
                attribute_type_test["spectrum"], attribute_type_test["type"], "spectrum"
            )

    def test_image_R(self):
        for attribute_type_test in self.ATTRIBUTE_TYPE_TESTS:
            self.read_R_test(
                attribute_type_test["image"], attribute_type_test["type"], "image"
            )

    def test_image_RW(self):
        for attribute_type_test in self.ATTRIBUTE_TYPE_TESTS:
            self.read_RW_test(
                attribute_type_test["image"], attribute_type_test["type"], "image"
            )

    def test_image_W(self):
        for attribute_type_test in self.ATTRIBUTE_TYPE_TESTS:
            self.write_RW_test(
                attribute_type_test["image"], attribute_type_test["type"], "image"
            )

    def test_image_readback(self):
        for attribute_type_test in self.ATTRIBUTE_TYPE_TESTS:
            self.readback_test(
                attribute_type_test["image"], attribute_type_test["type"], "image"
            )


class TestAttributeAccess(testscenarios.WithScenarios, TestCase):
    class float32_scalar_device(DeviceWrapper):
        scalar_R = AttributeWrapper(
            comms_annotation="float32_scalar_R", datatype=numpy.float32
        )
        scalar_RW = AttributeWrapper(
            comms_annotation="float32_scalar_RW",
            datatype=numpy.float32,
            access=AttrWriteType.READ_WRITE,
        )

        spectrum_RW = AttributeWrapper(
            comms_annotation="spectrum_RW",
            dims=(3,),
            datatype=numpy.float32,
            access=AttrWriteType.READ_WRITE,
        )
        image_RW = AttributeWrapper(
            comms_annotation="image_RW",
            dims=(3, 2),
            datatype=numpy.float32,
            access=AttrWriteType.READ_WRITE,
        )

        @command()
        def initialise(self):
            self.configure_for_initialise()
            self.set_state(DevState.STANDBY)

    def test_cannot_access_before_initialise(self):
        with DeviceTestContext(self.float32_scalar_device, process=True) as proxy:
            with self.assertRaises(DevFailed):
                _ = proxy.scalar_R

    def test_can_access_after_initialise(self):
        with DeviceTestContext(self.float32_scalar_device, process=True) as proxy:
            proxy.initialise()

            _ = proxy.scalar_R

    def test_write_correct_dims_spectrum(self):
        with DeviceTestContext(self.float32_scalar_device, process=True) as proxy:
            proxy.initialise()

            proxy.spectrum_RW = [1.0, 2.0, 3.0]

    def test_write_wrong_dims_spectrum(self):
        with DeviceTestContext(self.float32_scalar_device, process=True) as proxy:
            proxy.initialise()

            with self.assertRaises(DevFailed):
                proxy.spectrum_RW = [1.0]

    def test_write_correct_dims_image(self):
        with DeviceTestContext(self.float32_scalar_device, process=True) as proxy:
            proxy.initialise()

            proxy.image_RW = [[1.0, 2.0], [2.0, 3.0], [3.0, 4.0]]

    def test_write_wrong_dims_image(self):
        with DeviceTestContext(self.float32_scalar_device, process=True) as proxy:
            proxy.initialise()

            with self.assertRaises(DevFailed):
                proxy.image_RW = [[1.0, 2.0], [2.0, 3.0]]
