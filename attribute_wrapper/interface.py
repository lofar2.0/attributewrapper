# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging

logger = logging.getLogger()

__all__ = ["AttributeWrapperInterface"]


class AttributeWrapperInterface:
    def __init__(self):
        """prepare the caches for attribute wrapper objects"""
        self._attribute_wrapper_io = {}
