# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging
from functools import reduce
from operator import mul

from tango import AttrWriteType, AttReqType
from tango.server import attribute

from attribute_wrapper.attribute_io import AttributeIO

__all__ = ["AttributeWrapper"]

logger = logging.getLogger()


class AttributeWrapper(attribute):
    """Wraps attribute to generate function creation"""

    def __init__(
        self,
        comms_id=None,
        comms_annotation=None,
        datatype=None,
        dims=(1,),
        access=AttrWriteType.READ,
        **kwargs,
    ):
        """
        wraps around the tango Attribute class.

        Provides an easier interface for 1d or 2d arrays. Also provides a way to
        abstract managing the communications interface.

        comms_id: user-supplied identifier that is attached to this object, to identify
                  which communication class will need to be attached
        comms_annotation: data passed along to the attribute. can be given any form of
                          data. handling is up to client implementation
        datatype: any numpy datatype
        dims: dimensions of the attribute as a tuple, or (1,) for a scalar.
        init_value: value
        """

        # ensure the type is a numpy array.
        # see also
        # https://pytango.readthedocs.io/en/stable/server_api/server.html?highlight=devlong#module-tango.server
        # for more details about type conversion Python/numpy -> PyTango
        if "numpy" not in str(datatype) and datatype != str and datatype != bool:
            raise ValueError(
                f"Attribute needs to be a Tango-supported numpy, str or bool type, but "
                f"has type {datatype}"
            )

        # store data that can be used to identify the comms interface to use. not
        # used by the wrapper itself
        self.comms_id = comms_id
        # store data that can be used by the comms interface. not used by the
        # wrapper itself
        self.comms_annotation = comms_annotation

        self.datatype = datatype

        if dims == (1,):
            # scalar
            # Tango defines a scalar as having dimensions (1,0), see
            # https://pytango.readthedocs.io/en/stable/server_api/attribute.html
            max_dim_x = 1
            max_dim_y = 0
            dtype = datatype
            shape = ()
        elif len(dims) == 1:
            # spectrum
            max_dim_x = dims[0]
            max_dim_y = 0
            dtype = (datatype,)
            shape = (max_dim_x,)
        elif len(dims) == 2:
            # image
            max_dim_x = dims[1]
            max_dim_y = dims[0]
            dtype = ((datatype,),)
            shape = (max_dim_y, max_dim_x)
        else:
            # higher dimensional, >2D arrays collapse into the X and Y dimensions.
            # The Y (major) dimension mirrors the first dimension given, the
            # rest collapses into the X (minor) dimension.
            max_dim_x = reduce(mul, dims[1:])
            max_dim_y = dims[0]
            dtype = ((datatype,),)
            shape = (max_dim_y, max_dim_x)

        # actual shape of the data as it is read/written
        self.shape = shape

        if access == AttrWriteType.READ_WRITE:
            """If the attribute is of READ_WRITE type, assign the write and read
            functions to it"""

            # we return the last written value, as we are the only ones in control,
            # and the hardware does not necessarily return what we've written
            # (see L2SDP-725).

            def write_func_wrapper(device, value):
                """write_func_wrapper writes a value to this attribute"""

                try:
                    io = self.get_attribute_io(device)

                    return io.cached_write_function(value)
                except Exception as e:
                    raise e.__class__(
                        f"Could not write attribute {comms_annotation}"
                    ) from e

            def read_func_wrapper(device):
                """
                read_func_wrapper reads the attribute value, stores it and returns it"
                """

                # lofar.read_attribute ignores fisallowed. So check again if we're
                # allowed to read.
                if not device.is_attribute_access_allowed(AttReqType.READ_REQ):
                    return None

                try:
                    io = self.get_attribute_io(device)

                    return io.cached_read_function()
                except Exception as e:
                    raise e.__class__(
                        f"Could not read attribute {comms_annotation}"
                    ) from e

            self.fset = write_func_wrapper
            self.fget = read_func_wrapper
        else:
            """Assign the read function to the attribute"""

            def read_func_wrapper(device):
                """
                read_func_wrapper reads the attribute value, stores it and returns it"
                """

                # lofar.read_attribute ignores fisallowed. So check again if we're
                # allowed to read.
                if not device.is_attribute_access_allowed(AttReqType.READ_REQ):
                    return None

                try:
                    io = self.get_attribute_io(device)

                    return io.read_function()
                except Exception as e:
                    raise e.__class__(
                        f"Could not read attribute {comms_annotation}"
                    ) from e

            self.fget = read_func_wrapper

        # "fisallowed" is called to ask us whether an attribute can be accessed. If not,
        # the attribute won't be accessed, and the cache not updated. This forces Tango
        # to also force a read the moment an attribute does become accessible.
        # The provided function will be used with the call signature:
        #   "(device: Device, req_type: AttReqType) -> bool".
        #
        # NOTE: fisallowed=<callable> does not work:
        # https://gitlab.com/tango-controls/pytango/-/issues/435
        # So we have to use fisallowed=<str> here, which causes the function
        # device.<str> to be called.
        super().__init__(
            dtype=dtype,
            max_dim_y=max_dim_y,
            max_dim_x=max_dim_x,
            access=access,
            fisallowed="is_attribute_access_allowed",
            format=str(dims),
            **kwargs,
        )

    def get_attribute_io(self, device):
        """Returns the attribute I/O functions from device"""

        try:
            return device._attribute_wrapper_io[self]
        except KeyError:
            device._attribute_wrapper_io[self] = AttributeIO(device, self)
            return device._attribute_wrapper_io[self]

    def set_comm_client(self, device, client):
        """Takes communication client with 'get_mapping' function

        takes a communications client as input arguments This client should be of a
        class containing a "get_mapping" function and return a read and write function
        that the wrapper will use to get/set data.
        """
        try:
            read_attr_func, write_attr_func = client.setup_attribute(
                self.comms_annotation, self
            )

            io = self.get_attribute_io(device)
            io.read_function = read_attr_func
            io.write_function = write_attr_func
        except Exception as e:
            raise Exception(
                f"Exception while setting {client.__class__.__name__} attribute with "
                f"annotation: '{self.comms_annotation}'"
            ) from e

    async def async_set_comm_client(self, device, client):
        """Asynchronous version of set_comm_client."""
        try:
            read_attr_func, write_attr_func = await client.setup_attribute(
                self.comms_annotation, self
            )

            io = self.get_attribute_io(device)
            io.read_function = read_attr_func
            io.write_function = write_attr_func
        except Exception as e:
            raise Exception(
                f"Exception while setting {client.__class__.__name__} attribute with "
                f"annotation: '{self.comms_annotation}'"
            ) from e

    def set_pass_func(self, device):
        logger.debug(
            "using pass function for attribute with annotation: {}".format(
                self.comms_annotation
            )
        )

        io = self.get_attribute_io(device)
        io.read_function = lambda: None
        io.write_function = lambda value: None
