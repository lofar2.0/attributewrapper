# Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import logging

import numpy

__all__ = ["AttributeIO"]

logger = logging.getLogger()


class AttributeIO(object):
    """Holds the I/O functionality for an attribute for a specific device."""

    def __init__(self, device, attribute_wrapper):
        # Link to the associated device
        self.device = device

        # Link to the associated attribute wrapper
        self.attribute_wrapper = attribute_wrapper

        # Link to last (written) value
        self.cached_value = None

        # Specific read and write functions for this attribute on this device
        self.read_function = lambda: None
        self.write_function = lambda value: None

    def cached_read_function(self):
        """Return the last (written) value, if available. Otherwise, read
        from the device."""

        if self.cached_value is not None:
            return self.cached_value

        self.cached_value = self.read_function()
        return self.cached_value

    def cached_write_function(self, value):
        """Writes the given value to the device, and updates the cache."""

        # flexible array sizes are not supported by all clients. make sure we only
        # write arrays of maximum size.
        if self.attribute_wrapper.shape != ():
            if isinstance(value, numpy.ndarray):
                value_shape = value.shape
            else:
                if len(value) > 0 and isinstance(value[0], list):
                    # nested list
                    value_shape = (len(value), len(value[0]))
                else:
                    # straight list
                    value_shape = (len(value),)

            if value_shape != self.attribute_wrapper.shape:
                raise ValueError(
                    f"Tried writing an array of shape {value_shape} into an attribute "
                    f"of shape {self.attribute_wrapper.shape}"
                )

        self.write_function(value)
        self.cached_value = value
