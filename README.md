[![Pipeline Status](https://git.astron.nl/lofar2.0/attributewrapper/badges/main/pipeline.svg)](https://git.astron.nl/lofar2.0/attributewrapper/-/pipelines)
[![Coverage Status](https://git.astron.nl/lofar2.0/attributewrapper/badges/main/coverage.svg)](https://git.astron.nl/lofar2.0/attributewrapper/-/jobs/artifacts/main/download?job=coverage)
[![Python Versions](https://img.shields.io/badge/python-3.7%20|%203.8%20|%203.9%20|%203.10%20|%203.11-informational)](https://git.astron.nl/lofar2.0/attributewrapper)
[![Latest Documentation](https://img.shields.io/badge/docs-download-informational)](https://git.astron.nl/lofar2.0/attributewrapper/-/jobs/artifacts/main/download?job=package_docs)
[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Latest Release](https://git.astron.nl/lofar2.0/attributewrapper/-/badges/release.svg)](https://git.astron.nl/lofar2.0/attributewrapper/-/releases)

# PyTango Attribute Wrapper

The PyTango attribute wrapper reduces boilerplate code for pytango devices
by generating attribute read and write functions dynamically.

## Installation
```
pip install .
```

## Usage


## Contributing

To contribute, please create a feature branch and a "Draft" merge request.
Upon completion, the merge request should be marked as ready and a reviewer
should be assigned.

Verify your changes locally and be sure to add tests. Verifying local
changes is done through `tox`.

```pip install tox```

With tox the same jobs as run on the CI/CD pipeline can be ran. These
include unit tests and linting.

```tox```

To automatically apply most suggested linting changes execute:

```tox -e format```

## License
This project is licensed under the Apache License Version 2.0

## Releases

- 0.3.1 - Do not go to FAULT if a read fails
- 0.3 - Do not go to FAULT if a write fails
- 0.2 - Ensure requirements.txt dependencies are installed
- 0.1 - Initial release from separating into own repository
